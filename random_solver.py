# solver -- solver interface

import random
from solver import Solver
from words import words

class RandomSolver(Solver):

    def __init__(self):
        pass

    def next_guess(self, guesses, clues):
        return random.choice(words)
