# test_puzzle.py -- unit test for the puzzle module

from puzzle import Puzzle

def test_constructor():
    p = Puzzle("mauve")
    answer = p.i_give_up()
    assert answer == "mauve"

def test_empty_constructor():
    p = Puzzle()
    answer = p.i_give_up()
    assert isinstance(answer, str)
    assert len(answer) == 5

def test_guess():
    p = Puzzle("noose")
    clue = p.guess("aspen")
    assert isinstance(clue, list)
    assert clue == [None, Puzzle.YELLOW, None, Puzzle.YELLOW, Puzzle.YELLOW]

def test_guess_with_match():
    p = Puzzle("dings")
    clue = p.guess("tiled")
    assert isinstance(clue, list)
    assert clue == [None, Puzzle.GREEN, None, None, Puzzle.YELLOW]

def test_guess_limit():
    p = Puzzle("humor")
    guesses = ["jihad", "wooly", "saves", "graph", "befog", "dosed"]
    for guess in guesses:
        clue = p.guess(guess)

    err = None

    try:
        clue = p.guess("actor")
    except Exception as e:
        err = e

    assert isinstance(err, Exception), "Puzzle did not enforce 6-guess limit"

def test_no_guesses_after_giving_up():

    p = Puzzle()
    answer = p.i_give_up()

    try:
        clue = p.guess(answer)
    except Exception as e:
        err = e

    assert isinstance(err, Exception), "Puzzle did not enforce giving up limit"
    assert not isinstance(err, AssertionError)

def test_guess_must_be_a_word():

    p = Puzzle()

    err = None

    try:
        answer = p.guess("aaaaa")
    except Exception as e:
        err = e

    assert isinstance(err, Exception), "Puzzle did not enforce word list requirement"

def test_constructor_argument_must_be_a_word():

    err = None

    try:
        p = Puzzle("aaaaa")
    except Exception as e:
        err = e

    assert isinstance(err, Exception), "Puzzle did not enforce word list requirement for constructor"

def test_double_letters_in_guess():

    p = Puzzle("manor")
    clue = p.guess("goose")
    assert clue[1] == Puzzle.YELLOW
    assert clue[2] is None

def test_triple_letters_in_guess():

    p = Puzzle("elder")
    clue = p.guess("geese")
    assert clue[1] == Puzzle.YELLOW
    assert clue[2] == Puzzle.YELLOW
    assert clue[4] is None

def test_double_letters_in_guess_with_match():

    p = Puzzle("stole")
    clue = p.guess("tooth")
    assert clue[1] is None
    assert clue[2] is Puzzle.GREEN

def test_double_letters_in_solution():

    p = Puzzle("beets")
    clue = p.guess("cater")
    assert clue[3] == Puzzle.YELLOW

def test_double_letters_in_guess_and_solution():

    p = Puzzle("deeds")
    clue = p.guess("odder")
    assert clue[1] == Puzzle.YELLOW
    assert clue[2] == Puzzle.YELLOW

def test_double_letters_exact_match():

    p = Puzzle("promo")
    clue = p.guess("promo")
    assert all(item is Puzzle.GREEN for item in clue)

def test_deuce_leech():

    p = Puzzle("deuce")
    clue = p.guess("leech")
    assert clue[2] == Puzzle.YELLOW
