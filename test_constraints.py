# test_constraints.py -- test the constraints class

from constraints import Constraints
from puzzle import Puzzle

def test_constructor():
    c = Constraints([], [])
    assert isinstance(c, Constraints)

def test_meets():
    p = Puzzle("ensue")
    clue = p.guess("small")
    c = Constraints(["small"], [clue])
    assert c.meets("geese")
    assert not c.meets("louse")

def test_double_letters_with_match():
    p = Puzzle("nukes")
    clue = p.guess("jewel")
    c = Constraints(["jewel"], [clue])
    assert c.max_count["e"] == 1
    assert c.min_count["e"] == 1
    assert c.max_count["j"] == 0
    assert c.max_count["l"] == 0
    assert c.max_count["w"] == 0
    assert c.matched[3] == "e"

def test_double_letters_in_guess():

    p = Puzzle("manor")
    clue = p.guess("goose")
    c = Constraints(["goose"], [clue])
    assert c.max_count["o"] == 1
    assert c.min_count["o"] == 1
    assert c.max_count["g"] == 0
    assert c.max_count["s"] == 0
    assert c.max_count["e"] == 0
    assert all(match is None for match in c.matched)
    assert c.meets("manor")

def test_triple_letters_in_guess():

    p = Puzzle("elder")
    clue = p.guess("geese")
    c = Constraints(["geese"], [clue])
    assert c.max_count["e"] == 2
    assert c.min_count["e"] == 2
    assert c.max_count["g"] == 0
    assert c.max_count["s"] == 0
    assert ("e", 1) in c.wrong_spot
    assert ("e", 2) in c.wrong_spot
    assert ("e", 4) in c.wrong_spot
    assert all(match is None for match in c.matched)
    assert c.meets("elder")

def test_double_letters_in_guess_with_match():

    p = Puzzle("stole")
    clue = p.guess("tooth")
    c = Constraints(["tooth"], [clue])
    assert c.max_count["o"] == 1
    assert c.min_count["o"] == 1
    assert c.max_count["t"] == 1
    assert c.min_count["t"] == 1
    assert c.max_count["h"] == 0
    assert ("t", 0) in c.wrong_spot
    assert ("t", 3) in c.wrong_spot
    assert ("o", 1) in c.wrong_spot
    assert c.matched[2] == "o"
    assert c.meets("stole")

def test_double_letters_in_solution():

    p = Puzzle("beets")
    clue = p.guess("cater")
    c = Constraints(["cater"], [clue])
    assert c.min_count["e"] == 1
    assert "e" not in c.max_count.keys()
    assert c.max_count["c"] == 0
    assert c.max_count["a"] == 0
    assert c.min_count["t"] == 1
    assert c.max_count["r"] == 0
    assert ("e", 3) in c.wrong_spot
    assert ("t", 2) in c.wrong_spot
    assert c.meets("beets")

def test_double_letters_in_guess_and_solution():

    p = Puzzle("deeds")
    clue = p.guess("odder")
    c = Constraints(["odder"], [clue])
    assert c.min_count["d"] == 2
    assert "d" not in c.max_count.keys()
    assert ("d", 1) in c.wrong_spot
    assert ("d", 2) in c.wrong_spot
    assert c.min_count["e"] == 1
    assert "e" not in c.max_count
    assert c.max_count["o"] == 0
    assert c.max_count["r"] == 0
    assert c.meets("deeds")

def test_double_letters_exact_match():

    p = Puzzle("promo")
    clue = p.guess("promo")
    c = Constraints(["promo"], [clue])
    assert c.min_count["p"] == 1
    assert c.min_count["r"] == 1
    assert c.min_count["o"] == 2
    assert c.min_count["m"] == 1
    assert c.matched[0] == "p"
    assert c.matched[1] == "r"
    assert c.matched[2] == "o"
    assert c.matched[3] == "m"
    assert c.matched[4] == "o"
    assert c.meets("promo")

def test_problem_path_1():

    p = Puzzle("hooch")
    guesses = ['fatty', 'robed', 'logos', 'couch']
    clues = []
    for guess in guesses:
        clues.append(p.guess(guess))

    c = Constraints(guesses, clues)

    assert c.meets("hooch")

def test_problem_path_2():

    p = Puzzle("deuce")
    guesses = ['scoff', 'leech', 'mecca']
    clues = []
    for guess in guesses:
        clues.append(p.guess(guess))

    c = Constraints(guesses, clues)

    assert c.meets("deuce")

def test_problem_path_3():
    p = Puzzle("chaff")
    guesses = ['globs', 'friar', 'taffy', 'chafe']
    clues = []
    for guess in guesses:
        clues.append(p.guess(guess))

    c = Constraints(guesses, clues)

    assert c.meets("chaff")
