# solver -- solver interface

class Solver:

    def __init__(self):
        pass

    def next_guess(self, guesses, clues):
        raise NotImplementedError
