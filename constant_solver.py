# constant_solver -- ConstantSolver strategy

import random
from solver import Solver
from words import words

class ConstantSolver(Solver):

    def __init__(self, word=None):
        if word is None:
            self.__word = random.choice(words)
        else:
            self.__word = word

    def next_guess(self, guesses, clues):
        return self.__word
