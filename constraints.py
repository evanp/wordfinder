from puzzle import Puzzle

def empty_alphabet_dictionary():
    alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    d = dict()
    for letter in alphabet:
        d[letter] = 0
    return d

class Constraints:

    def __init__(self, guesses, clues):

        assert len(guesses) == len(clues)
        assert all(isinstance(guess, str) for guess in guesses)
        assert all(len(guess) == 5 for guess in guesses)
        assert all(isinstance(clue, list) for clue in clues)
        assert all(len(clue) == 5 for clue in clues)

        self.matched = [None] * 5
        self.wrong_spot = set()
        self.min_count = dict()
        self.max_count = dict()

        for i, clue in enumerate(clues):
            guess = guesses[i]

            yellows = empty_alphabet_dictionary()
            greens = empty_alphabet_dictionary()
            blacks = empty_alphabet_dictionary()

            for j, v in enumerate(clue):
                letter = guess[j]
                if v == Puzzle.GREEN:
                    self.matched[j] = letter
                    greens[letter] += 1
                elif v == Puzzle.YELLOW:
                    self.wrong_spot.add((letter, j))
                    yellows[letter] += 1
                elif v is None:
                    self.wrong_spot.add((letter, j))
                    blacks[letter] += 1

            for letter in set(guess):
                if blacks[letter] > 0:
                    self.max_count[letter] = yellows[letter] + greens[letter]

                if yellows[letter] > 0 or greens[letter] > 0:
                    min_count = yellows[letter] + greens[letter]
                    if letter not in self.min_count or self.min_count[letter] < min_count:
                        self.min_count[letter] = min_count

    def meets(self, word):

        for i, letter in enumerate(word):
            if self.matched[i] is not None and self.matched[i] != letter:
                return False
            elif (letter, i) in self.wrong_spot:
                return False

        for letter, min_count in self.min_count.items():
            if word.count(letter) < min_count:
                return False

        for letter, max_count in self.max_count.items():
            if word.count(letter) > max_count:
                return False

        return True
