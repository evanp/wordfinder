# test_startword_solver.py -- test the startword solver class

from startword_solver import StartwordSolver
from solver import Solver
from puzzle import Puzzle

def is_success(clue):
    return all(result == Puzzle.GREEN for result in clue)

def test_constructor():
    sws = StartwordSolver()
    assert isinstance(sws, StartwordSolver)
    assert isinstance(sws, Solver)

def test_solve_known():
    sws = StartwordSolver("rouse")
    p = Puzzle("known")
    guesses = []
    clues = []
    success = False
    for i in range(0, 6):
        guess = sws.next_guess(guesses, clues)
        assert isinstance(guess, str)
        clue = p.guess(guess)
        if (is_success(clue)):
            success = True
            break
        else:
            guesses.append(guess)
            clues.append(clue)
    assert True

def test_solve_many():
    sws = StartwordSolver("chain")
    for n in range(0, 10):
        p = Puzzle()
        guesses = []
        clues = []
        success = False
        for i in range(0, 6):
            guess = sws.next_guess(guesses, clues)
            assert isinstance(guess, str)
            clue = p.guess(guess)
            if (is_success(clue)):
                success = True
                break
            else:
                guesses.append(guess)
                clues.append(clue)
    assert True
