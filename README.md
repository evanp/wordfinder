# Wordfinder

These are my noodlings with automatically solving [Wordle](https://www.nytimes.com/games/wordle/index.html).

They're dedicated to the public domain using [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

Rough idea of what's in the repo:

- words.py. This is the list of available 5-letter words as a big Python list.
- puzzle.py. This represents a Wordle-like guessing game puzzle.
- solver.py. Abstract class for an automated solver of the puzzle.
- constant_solver.py. Just tries the same guess over and over. Very bad!
- random_solver.py. Just guesses random words. Also very bad!
- constraint_solver.py. Uses the information from puzzle to constrain the list
  of possible guesses. Then, picks randomly.
- constraints.py. Represents a set of constraints, and matches words that meet
  those constraints.
- eliminative_solver.py. Uses letter frequency information on candidate words
  to try to reduce the next set of candidate words as much as possible. Tries to
  bisect the candidate list.
- startword_solver.py. EliminativeSolver that you can provide a startword to.
- performance.py. Evaluates the performance of a solver, including the median
  path length of the solutions.
- startword_value.py. Given a startword, use it to solve all the possible words
  in the word list, and give the median path length.
- find_best_startword.py. Go through all the available words and find the ten  that give the best results. Only useful if you can manage the rest of the algorithm correctly. 
