# test_eliminative_solver.py -- test the eliminative solver class

from eliminative_solver import EliminativeSolver
from solver import Solver
from puzzle import Puzzle

def is_success(clue):
    return all(result == Puzzle.GREEN for result in clue)

def test_constructor():
    es = EliminativeSolver()
    assert isinstance(es, EliminativeSolver)
    assert isinstance(es, Solver)

def test_solve_known():
    es = EliminativeSolver()
    p = Puzzle("known")
    guesses = []
    clues = []
    success = False
    for i in range(0, 6):
        guess = es.next_guess(guesses, clues)
        assert isinstance(guess, str)
        clue = p.guess(guess)
        if (is_success(clue)):
            success = True
            break
        else:
            guesses.append(guess)
            clues.append(clue)
    assert True

def test_solve_many():
    es = EliminativeSolver()
    for n in range(0, 10):
        p = Puzzle()
        guesses = []
        clues = []
        success = False
        for i in range(0, 6):
            guess = es.next_guess(guesses, clues)
            assert isinstance(guess, str)
            clue = p.guess(guess)
            if (is_success(clue)):
                success = True
                break
            else:
                guesses.append(guess)
                clues.append(clue)
    assert True
