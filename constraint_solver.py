# constraint_solver.py -- Use what we know to guess at words

import random
from solver import Solver
from words import words
from constraints import Constraints

class NoCandidatesError(Exception):

    def __init__(self, constraints, guesses, clues):
        self.constraints = constraints
        self.guesses = guesses
        self.clues = clues

class ConstraintSolver(Solver):

    def __init__(self):
        pass

    def next_guess(self, guesses, clues):
        if len(guesses) == 0:
            self.prev = None
            prev = words
        else:
            prev = self.prev
        c = Constraints(guesses, clues)
        candidates = []
        for word in prev:
            if c.meets(word):
                candidates.append(word)
        if len(candidates) == 0:
            raise NoCandidatesError(c, guesses, clues)
        self.prev = candidates
        guess = self.choose_candidate(candidates, guesses, clues)
        return guess

    def choose_candidate(self, candidates, guesses, clues):
        if len(candidates) == 0:
            return None
        else:
            return random.choice(candidates)
