# find_best_startword.py -- find the best startword for our dataset

from performance import Performance
from puzzle import Puzzle
from startword_solver import StartwordSolver
from constraint_solver import ConstraintSolver
from words import words
import sys
import random

def is_success(clue):
    return all(result == Puzzle.GREEN for result in clue)

def startword_median_path(startword):
    ss = StartwordSolver(startword)
    stats = [0] * 7
    for word in random.sample(words, len(words)):
        p = Puzzle(word)
        guesses = []
        clues = []
        for i in range(0, 6):
            guess = ss.next_guess(guesses, clues)
            guesses.append(guess)
            clue = p.guess(guess)
            clues.append(clue)
            if is_success(clue):
                break
        if len(clues) > 0 and is_success(clues[len(clues) - 1]):
            stats[len(guesses)-1] += 1
        else:
            stats[6] += 1
    return Performance.median_path_length(stats)

def find_best_startword():
    top_10 = []
    top_10_paths = []
    for word in random.sample(words, len(words)):
        path = startword_median_path(word)
        print(f'{word}: {path}')
        n = next((i for i, v in enumerate(top_10_paths) if v > path), None)
        if n is not None:
            top_10_paths.insert(n, path)
            top_10_paths = top_10_paths[0:10]
            top_10.insert(n, word)
            top_10 = top_10[0:10]
        elif len(top_10_paths) < 10:
            top_10_paths.append(path)
            top_10.append(word)
        print(f'Top 10: {top_10}')
    return top_10

tops = find_best_startword()
print(f'Final top 10: {tops}')
