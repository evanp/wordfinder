# test_performance.py -- test the performance class does the right thing

from performance import Performance
from constraint_solver import ConstraintSolver

def test_performance():
    p = Performance(ConstraintSolver, 10)
    stats = p.measure()
    assert len(stats) == 7
    assert all(isinstance(n, int) for n in stats)
    assert all(0 <= n <= 100 for n in stats)

def test_median_path_length():
    p = Performance(ConstraintSolver, 10)
    stats = p.measure()
    length = Performance.median_path_length(stats)
    assert isinstance(length, float)
    assert 0 < length < 8
