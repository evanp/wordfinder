# performance.py -- determine the performance of a solver

import statistics
from puzzle import Puzzle

class Performance:

    def __init__(self, cls, n):
        self.cls = cls
        self.n = n

    def is_success(self, clue):
        return all(result == Puzzle.GREEN for result in clue)

    def measure(self):
        stats = [0] * 7
        for i in range(0, self.n):
            solver = self.cls()
            p = Puzzle()
            guesses = []
            clues = []
            for i in range(0, 6):
                try:
                    guess = solver.next_guess(guesses, clues)
                except:
                    answer = p.i_give_up()
                    print(f'Exception for {answer}, previous guesses {guesses}')
                    break
                guesses.append(guess)
                clue = p.guess(guess)
                clues.append(clue)
                if (self.is_success(clue)):
                    break
            if self.is_success(clue):
                stats[len(guesses)-1] += 1
            else:
                stats[6] += 1
        return stats

    def median_path_length(stats):
        results = []
        for i in range(0, 7):
            results = results + [float(i + 1)] * stats[i]
        mp = statistics.median_grouped(results)
        if mp > 6.0:
            return None
        else:
            return mp

if __name__ == "__main__":
    import sys
    import importlib

    if len(sys.argv) >= 3:
        n = int(sys.argv[2])
    else:
        n = 10

    if len(sys.argv) >= 2:
        solver_type = sys.argv[1]
    else:
        solver_type = "constraint"

    print(f'running solver {solver_type} {n} times...')

    module_name = f'{solver_type}_solver'
    module = importlib.import_module(module_name)
    class_name = f'{solver_type.title()}Solver'
    cls = module.__getattribute__(class_name)

    p = Performance(cls, n)
    stats = p.measure()

    print(f'Done.')

    for i, stat in enumerate(stats):
        if (i < 6):
            print(f'{i}: {stat}')
        else:
            print(f'X: {stat}')

    path = Performance.median_path_length(stats)

    print(f'Median path length: {path}')
