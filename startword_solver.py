# startword_solver.py -- use a good startword

import random
from eliminative_solver import EliminativeSolver

class StartwordSolver(EliminativeSolver):

    def __init__(self, word=None):
        self.startword = word

    def choose_candidate(self, candidates, guesses, clues):
        if len(guesses) == 0 and self.startword is not None:
            return self.startword
        else:
            return super().choose_candidate(candidates, guesses, clues)
