import random
from words import words

class Puzzle:

    GREEN = "green"
    YELLOW = "yellow"

    def __init__(self, word=None):

        if word is None:
            self.__word = Puzzle.__choose_word()
        elif not Puzzle.__is_word(word):
            raise Exception("not a word")
        else:
            self.__word = word

        self.__gave_up = False
        self.__guesses = 0

    def __choose_word():
        return random.choice(words)

    def __is_word(word):
        try:
            n = words.index(word)
            return True
        except ValueError:
            return False

    def guess(self, word):

        if self.__gave_up:
            raise Exception("Already gave up")

        if self.__guesses >= 6:
            raise Exception("Too many guesses")

        if not Puzzle.__is_word(word):
            raise Exception("That's not a word")

        assert len(word) == 5
        assert len(self.__word) == 5

        clue = [None, None, None, None, None]
        greens = {}
        yellows = {}

        # Colour exact matches green

        for i, v in enumerate(word):
            if v == self.__word[i]:
                clue[i] = Puzzle.GREEN
                if v in greens:
                    greens[v] += 1
                else:
                    greens[v] = 1

        # Colour misplaced matches yellow

        for letter in set(word):
            yellows[letter] = self.__word.count(letter)
            if letter in greens:
                yellows[letter] -= greens[letter]

        for i, v in enumerate(word):
            if self.__word[i] != v and v in yellows and yellows[v] > 0:
                clue[i] = Puzzle.YELLOW
                yellows[v] -= 1

        self.__guesses += 1

        return clue

    def i_give_up(self):
        self.__gave_up = True
        return self.__word
