# eliminative_solver.py -- Use what we know to guess at words
import random
from constraint_solver import ConstraintSolver
from puzzle import Puzzle

class EliminativeSolver(ConstraintSolver):

    def choose_candidate(self, candidates, guesses, clues):
        total = len(candidates)
        if total == 1:
            return candidates[0]
        fixed = list()
        if len(clues) > 0:
            last = clues[len(clues) - 1]
            for i in range(0, 5):
                if last[i] == Puzzle.GREEN:
                    fixed.append(i)
        candidate = self.best_candidate(candidates, fixed)
        return candidate

    def best_candidate(self, candidates, fixed):
        if len(fixed) >= 4:
            return random.choice(candidates)
        counts = self.get_letter_counts(candidates)
        items = self.make_items(counts)
        items = list(filter(lambda item: item[1] not in fixed, items))
        best = items[0]
        if best[2] == 1:
            return random.choice(candidates)
        next = list(filter(lambda word: word[best[1]] == best[0], candidates))
        fixed.append(best[1])
        return self.best_candidate(next, fixed)

    def get_letter_counts(self, candidates):
        counts = [None] * 5
        for i in range(0, 5):
            counts[i] = dict()
        for word in candidates:
            for i, letter in enumerate(word):
                if letter in counts[i]:
                    counts[i][letter] += 1
                else:
                    counts[i][letter] = 1
        return counts

    def make_items(self, counts):
        items = list()
        for i in range(0, 5):
            for l in counts[i].keys():
                items.append((l, i, counts[i][l]))
        return list((reversed(sorted(items, key=lambda item: item[2]))))
