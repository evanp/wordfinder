from words import words

def test_words():
    assert isinstance(words, list)
    assert len(words) > 0
