# find_best_startword.py -- find the best startword for our dataset

from performance import Performance
from puzzle import Puzzle
from startword_solver import StartwordSolver
from constraint_solver import ConstraintSolver
from words import words
import sys

def is_success(clue):
    return all(result == Puzzle.GREEN for result in clue)

def startword_median_path(startword):
    ss = StartwordSolver(startword)
    stats = [0] * 7
    for word in words:
        p = Puzzle(word)
        guesses = []
        clues = []
        for i in range(0, 6):
            guess = ss.next_guess(guesses, clues)
            guesses.append(guess)
            clue = p.guess(guess)
            clues.append(clue)
            if is_success(clue):
                break
        if len(clues) > 0 and is_success(clues[len(clues) - 1]):
            stats[len(guesses)-1] += 1
        else:
            stats[6] += 1
    return Performance.median_path_length(stats)

print(startword_median_path(sys.argv[1]))
