# test_constant_solver.py -- test the constant solver class

from constant_solver import ConstantSolver
from solver import Solver
from puzzle import Puzzle

def test_constructor():
    cs = ConstantSolver()
    assert isinstance(cs, ConstantSolver)
    assert isinstance(cs, Solver)

def test_solve():
    cs = ConstantSolver("adorn")
    p = Puzzle("bushy")
    guesses = []
    clues = []
    for i in range(0, 6):
        guess = cs.next_guess(guesses, clues)
        assert guess == "adorn", "ConstantSolver didn't return constant value"
        clue = p.guess(guess)
        guesses.append(guess)
        clues.append(clue)
