# test_solver.py -- test the solver class

from solver import Solver
from puzzle import Puzzle

import inspect

def test_is_class():
    assert inspect.isclass(Solver)

def test_constructor():
    s = Solver()
    assert isinstance(s, Solver)

def test_solve():
    s = Solver()
    assert hasattr(s, "next_guess")
    assert callable(s.next_guess)

def test_next_guess_is_unimplemented():
    s = Solver()
    p = Puzzle()

    err = None

    try:
        s.next_guess([], [])
    except Exception as e:
        err = e

    assert isinstance(err, NotImplementedError)
