# test_constraint_solver.py -- test the constraint solver class

from constraint_solver import ConstraintSolver
from solver import Solver
from puzzle import Puzzle

def is_success(clue):
    return all(result == Puzzle.GREEN for result in clue)

def test_constructor():
    cs = ConstraintSolver()
    assert isinstance(cs, ConstraintSolver)
    assert isinstance(cs, Solver)

def test_solve_known():
    cs = ConstraintSolver()
    p = Puzzle("known")
    guesses = []
    clues = []
    success = False
    for i in range(0, 6):
        guess = cs.next_guess(guesses, clues)
        assert isinstance(guess, str)
        clue = p.guess(guess)
        if (is_success(clue)):
            success = True
            break
        else:
            guesses.append(guess)
            clues.append(clue)
    assert True

def test_solve_many():
    cs = ConstraintSolver()
    for n in range(0, 10):
        p = Puzzle()
        guesses = []
        clues = []
        success = False
        for i in range(0, 6):
            guess = cs.next_guess(guesses, clues)
            assert isinstance(guess, str)
            clue = p.guess(guess)
            if (is_success(clue)):
                success = True
                break
            else:
                guesses.append(guess)
                clues.append(clue)
    assert True
