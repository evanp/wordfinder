# test_random_solver.py -- test the random solver class

from random_solver import RandomSolver
from solver import Solver
from puzzle import Puzzle

def is_success(clue):
    return all(result == Puzzle.GREEN for result in clue)

def test_constructor():
    rs = RandomSolver()
    assert isinstance(rs, RandomSolver)
    assert isinstance(rs, Solver)

def test_solve():
    rs = RandomSolver()
    p = Puzzle()
    guesses = []
    clues = []
    success = False
    for i in range(0, 6):
        guess = rs.next_guess(guesses, clues)
        clue = p.guess(guess)
        if (is_success(clue)):
            success = True
            break
        else:
            guesses.append(guess)
            clues.append(clue)
    assert True
